package com.igymtrainer.app.igym_trainer.bluetooth;

import android.os.Handler;
import android.os.Message;

import com.igymtrainer.app.igym_trainer.device.DeviceObjectClass;

import java.util.ArrayList;

import io.github.georgiosgoniotakis.bluetoothwrapper.library.core.BTExplorer;
import io.github.georgiosgoniotakis.bluetoothwrapper.library.exceptions.BTDeviceNotFoundException;
import io.github.georgiosgoniotakis.bluetoothwrapper.library.interfaces.MessageCodes;
import io.github.georgiosgoniotakis.bluetoothwrapper.library.properties.Mode;

/**
 * Created by Papazian on 9/10/2017.
 */

public final class BluetoothCommunication {


    /**
     * A keyword for this class.
     */
    private final String TAG = getClass().getSimpleName();

    private static BluetoothCommunication btCS;

    /**
     * A {@link BTExplorer} instance to communicate with
     * the library
     */
    private BTExplorer btExplorer;



    private BluetoothCommunication() {
        btExplorer = BTExplorer.getInstance(btHandler); // Get Singleton Instance
    }

    public static synchronized BluetoothCommunication getInstance(){
        if(btCS == null){
            btCS = new BluetoothCommunication();
        }

        return btCS;
    }


    /**
     * connect to specific device with MAC
     *
     * @param MAC string for connection
     */
    public boolean connectToDevice(String MAC) {
        try {

            btExplorer.connect(MAC, Mode.SECURE);

        } catch (BTDeviceNotFoundException e) {
                    /* Device not available any more, inform the user*/
            return false;
        }
        return true;
    }

    /**
     * on disconnect from device
     */
    public void disconnectToDevice() {
        btExplorer.disconnect();
    }

    /**
     * that function sent data to Bluetooth device
     *
     * @param data string the data mast to sent
     * @return true in no exception
     */
    public void sendData(String data) {

        btExplorer.send(data);
    }


    /**
     * getPairedDevice
     * return only the correct device
     *
     * @return paired Device with name "IGym-Trainer-" ArrayList<DeviceObjectClass>
     */
    public ArrayList<DeviceObjectClass> getPairedDevice() {

        ArrayList<DeviceObjectClass> pairDevice = new ArrayList<>();
        String[][] btDevices = btExplorer.deviceList(true);

        if (btDevices != null && btDevices.length > 0) {

            for (String[] btDevice : btDevices) {
                DeviceObjectClass bluetoothDevice = new DeviceObjectClass(btDevice[1], btDevice[0]);

                if (bluetoothDevice.isCorrectDevice()) {
                    pairDevice.add(bluetoothDevice);
                }
            }
        }
        return pairDevice;
    }

    /**
     * Implements a handler to let the two classes communicate. In this way
     * the logic class can notify this class when an incoming message is
     * available and thus the UI is updated.
     */
    private final Handler btHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MessageCodes.MESSAGE_READ:
                    msg.getData().getString(MessageCodes.INCOMING_MESSAGE);
                    break;
            }
        }
    };

    public boolean isEnabled(){
        return btExplorer.isEnabled();
    }

    public boolean isSupported(){
        return btExplorer.isSupported();
    }

}
