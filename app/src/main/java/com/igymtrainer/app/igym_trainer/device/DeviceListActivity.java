package com.igymtrainer.app.igym_trainer.device;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.igymtrainer.app.igym_trainer.R;
import com.igymtrainer.app.igym_trainer.activities.DeviceSettingsActivity;
import com.igymtrainer.app.igym_trainer.bluetooth.BluetoothCommunication;

import java.util.ArrayList;

public class DeviceListActivity extends AppCompatActivity implements ListView.OnItemClickListener {

    private ListView deviceCustomListView = null;
    private CustomizedListView adapter;
    private SingletonClass singletonClass = SingletonClass.getInstance();

    /**
     * Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);

        setUpViewItems();

        ArrayList<DeviceObjectClass> ArrayOfDeviceList = null;
        ArrayOfDeviceList = setArrayList();
        singletonClass.setDeviceObjectClassArrayList(ArrayOfDeviceList);
        adapter = new CustomizedListView(DeviceListActivity.this, ArrayOfDeviceList);
        deviceCustomListView.setAdapter(adapter);


    }

    /**
     * Called when the activity is started and initialise view objects.
     */
    private void setUpViewItems() {
        deviceCustomListView = (ListView) findViewById(R.id.deviceCustomListView);
        deviceCustomListView.setOnItemClickListener(DeviceListActivity.this);
    }


    /**
     * On click go to Activity DeviceSettingsActivity and sent Data
     * sent device id to get values from that
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(DeviceListActivity.this, " id=" + id, Toast.LENGTH_SHORT).show();

        Intent sentIntent = new Intent(DeviceListActivity.this, DeviceSettingsActivity.class);
        sentIntent.putExtra("DEVICE_ID", position);
        startActivity(sentIntent);
    }


    /**
     * set device List of Objects DeviceObjectClass
     *
     * @return ArrayList
     */
    private ArrayList<DeviceObjectClass> setArrayList() {
        BluetoothCommunication btClass = BluetoothCommunication.getInstance();
        return btClass.getPairedDevice();
    }

}
