package com.igymtrainer.app.igym_trainer.device;

import java.util.ArrayList;

/**
 * Created by Papazian on 11/10/2017.
 */

public class SingletonClass {
    private static SingletonClass ourInstance;

    private ArrayList<DeviceObjectClass> deviceObjectClassArrayList=null;


    public static synchronized SingletonClass getInstance() {

        if(ourInstance == null){
            ourInstance = new SingletonClass();
        }

        return ourInstance;
    }

    private SingletonClass() {
    }

    public ArrayList<DeviceObjectClass> getDeviceObjectClassArrayList() {
        return deviceObjectClassArrayList;
    }

    public void setDeviceObjectClassArrayList(ArrayList<DeviceObjectClass> deviceObjectClassArrayList) {
        this.deviceObjectClassArrayList = deviceObjectClassArrayList;
    }
}
