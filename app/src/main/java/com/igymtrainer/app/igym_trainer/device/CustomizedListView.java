package com.igymtrainer.app.igym_trainer.device;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.igymtrainer.app.igym_trainer.R;
import com.igymtrainer.app.igym_trainer.device.DeviceObjectClass;

import java.util.ArrayList;

/**
 * Created by Papazian on 6/10/2017.
 */

public class CustomizedListView extends BaseAdapter{

    Context context;
    LayoutInflater layoutInflater;

    ArrayList<DeviceObjectClass> arrayListDeviceObjectClass=null;

    /**
     * Construction of CustomizedListView must set context and arrayList of Custom DeviceObjectClass
     * @param context Context
     * @param arrayListDeviceObjectClass ArrayList<DeviceObjectClass>
     */
   public CustomizedListView (Context context ,ArrayList<DeviceObjectClass> arrayListDeviceObjectClass){
       this.arrayListDeviceObjectClass=arrayListDeviceObjectClass;
       this.context=context;
       layoutInflater =(LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
   }


    /**
     * return device name for title
     * @param  id int
     * @return string
     */
   public String getDeviceTitle(int id){
       return arrayListDeviceObjectClass.get(id).getDeviceName();
   }

    /**
     * count objects of ths list
     * @return size int
     */
    @Override
    public int getCount() {
        return arrayListDeviceObjectClass.size();
    }

    /**
     * get object off array list
     * @param position int
     * @return  DeviceObjectClass
     */
    @Override
    public Object getItem(int position) {
        return arrayListDeviceObjectClass.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Set values to view objects
     * @param position int
     * @param convertView View
     * @param parent View.parent
     * @return View object
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.custom_list_view_item_for_device,null);
        TextView txtTitle =(TextView) convertView.findViewById(R.id.txtViewDeviseNameTitle);
        TextView txtSubString = (TextView) convertView.findViewById(R.id.txtViewDeviceSubString);
        ImageView imgViewForCustomListVie = (ImageView) convertView.findViewById(R.id.imgViewForListView);


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize=5;
        Bitmap bitmap = BitmapFactory.decodeResource(convertView.getResources(),R.drawable.logomakr31,options);
        imgViewForCustomListVie.setImageBitmap(bitmap);

        txtTitle.setText(arrayListDeviceObjectClass.get(position).getDeviceName());
        txtSubString.setText(arrayListDeviceObjectClass.get(position).getMAC());
        return convertView;
    }
}
