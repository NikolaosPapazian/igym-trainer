package com.igymtrainer.app.igym_trainer.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.igymtrainer.app.igym_trainer.R;
import com.igymtrainer.app.igym_trainer.bluetooth.BluetoothCommunication;
import com.igymtrainer.app.igym_trainer.device.DeviceListActivity;

import io.github.georgiosgoniotakis.bluetoothwrapper.library.interfaces.BTNotifiable;
import io.github.georgiosgoniotakis.bluetoothwrapper.library.receivers.BTReceivers;

public class MainActivity extends Activity implements View.OnClickListener, BTNotifiable {


    private BTReceivers btReceivers;

    private BluetoothCommunication btCom;

    private final String ERROR_CODE = "ERROR_CODE";


    /**
     * Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpViewItems();

        btCom = BluetoothCommunication.getInstance();

        btReceivers = new BTReceivers(this, true);
        btReceivers.registerReceivers();

    }

    /**
     * Called when the activity is started and initialise view objects.
     */
    private void setUpViewItems() {
        Button btnStartListActivity = (Button) findViewById(R.id.btn_start_list_activity);
        btnStartListActivity.setOnClickListener(this);
    }

    /**
     * Called when the button click and go to BluetoothObjectListActivity.
     *
     * @param viewObject View
     */
    @Override
    public void onClick(View viewObject) {
        if (viewObject.getId() == R.id.btn_start_list_activity) {

            //TODO: mast check for Bluetooth. have this device Bluetooth, is open, have pair device with name IGym-Trainer.
            if (!btCom.isSupported()) {
                changeActivity("No_Bluetooth");
            } else if (!btCom.isEnabled()) {
                changeActivity("Bluetooth_close");
            } else {
                startActivity(new Intent(MainActivity.this, DeviceListActivity.class));
            }

        }
    }

    private void changeActivity(String errorMessage) {
        Intent sentIntent = new Intent(this, ErrorDisplayActivity.class);
        sentIntent.putExtra(ERROR_CODE, errorMessage);
        startActivity(sentIntent);
    }

    /**
     * @return true if have paired device with name IGym-Trainer
     */
    public boolean hasDeviceWithCorrectName() {
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        btReceivers.unregisterReceivers();
    }


    @Override
    public void onAdapterStateChange(int adapterState) {
        if (adapterState == BluetoothAdapter.STATE_TURNING_OFF) {
            Toast.makeText(this, "Please re-enable bluetooth.", Toast.LENGTH_LONG).show();
        } else if (adapterState == BluetoothAdapter.STATE_TURNING_ON) {
            Toast.makeText(this, "Thank you for enabling you bluetooth.", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onDeviceStateChange(String deviceState) {
        if (deviceState.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
            Toast.makeText(this, "BT Device connected", Toast.LENGTH_LONG).show();
        } else if (deviceState.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
            Toast.makeText(this, "BT Device disconnected", Toast.LENGTH_LONG).show();
        }
    }
}
