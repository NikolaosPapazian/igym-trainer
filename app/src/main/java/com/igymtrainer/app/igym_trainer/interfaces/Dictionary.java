package com.igymtrainer.app.igym_trainer.interfaces;

/**
 * Use: String some = Dictionary.IDENTIFIER_LED;
 */
interface Dictionary {
    String IDENTIFIER_LED = "LED";
    String IDENTIFIER_LED_RED = "LED_R";
    String IDENTIFIER_LED_GREEN = "LED_G";
    String IDENTIFIER_LED_BLUE = "LED_B";
    String IDENTIFIER_MOD = "MOD";
    String IDENTIFIER_SEN = "SEN";
    String IDENTIFIER_TIM = "TIM";
    String IDENTIFIER_MIN_TIME = "MIN_TIM";
    String IDENTIFIER_MAX_TIME = "MAX_TIM";
    String IDENTIFIER_SINC = "SINC";
    String IDENTIFIER_POWER = "POWER";
    String IDENTIFIER_STATISTICS = "STATISTICS";
    String IDENTIFIER_DELIMETER = ":";
}
