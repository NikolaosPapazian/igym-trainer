package com.igymtrainer.app.igym_trainer.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.igymtrainer.app.igym_trainer.R;

public class SplashScreenActivity extends AppCompatActivity {

    private final String TAG = "SplashScreenActivity";

    private boolean active = true;

    private final int SPLASH_TIME = 1100; // time to display the splash screen in ms

    protected ImageView SplashScreenImageViewer = null;
    private int[] mImageIds = {
            R.drawable.logomakr31,
            R.drawable.logomakr32,
            R.drawable.logomakr33,
            R.drawable.logomakr34,
            R.drawable.logomakr35,
            R.drawable.logomakr36,
            R.drawable.logomakr37,
            R.drawable.logomakr38,
            R.drawable.logomakr39,
            R.drawable.logomakr310,
            R.drawable.logomakr311
    };
    private int cntImage = 0;


    /**
     * Called when the activity is first created.
     * <p>
     * call the setUpViewItems
     * call the changeImgDelayNewThread
     *
     * @param savedInstanceState Current Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        setUpViewItems();
        changeImgDelayNewThread();
    }

    /**
     * Called when the activity is started and initialise view objects.
     */
    private void setUpViewItems() {
        SplashScreenImageViewer = (ImageView) findViewById(R.id.SplashScreenImageViewer);
    }


    /**
     * Called when you wont to change image to imageView.
     *
     * @param index index is id for image table
     */
    private void showImage(int index) {
        if (SplashScreenImageViewer != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), mImageIds[index], options);
            SplashScreenImageViewer.setImageBitmap(bitmap);
        } else {
            Log.e(TAG, "Exception Image View error");
        }
    }

    //TODO:: Handler instead of sleep()
    /**
     * Starts a new thread and every 300ms call the "showImage" function wen finis starts MainActivity .
     */
    private void changeImgDelayNewThread() {
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (active && (waited < SPLASH_TIME)) {
                        showImage(cntImage);
                        if (cntImage < 10) {
                            cntImage++;
                        } else {
                            cntImage = 0;
                        }
                        sleep(500);
                        if (active) {
                            waited += 100;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Exception = ");
                    System.out.println(e.toString());
                } finally {

                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                    finish();
                }
            }
        };
        splashTread.start();
    }
}
