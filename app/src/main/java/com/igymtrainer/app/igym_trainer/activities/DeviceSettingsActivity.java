package com.igymtrainer.app.igym_trainer.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.igymtrainer.app.igym_trainer.bluetooth.BluetoothCommunication;
import com.igymtrainer.app.igym_trainer.device.DeviceObjectClass;
import com.igymtrainer.app.igym_trainer.R;
import com.igymtrainer.app.igym_trainer.device.SingletonClass;

import java.util.ArrayList;


public class DeviceSettingsActivity extends AppCompatActivity implements OnCheckedChangeListener,
        NumberPicker.OnValueChangeListener {

    private TextView txtTitle = null;
    private CheckBox checkOnOff = null;
    private RadioButton radioButtonColorPicker1 = null;
    private RadioButton radioButtonColorPicker2 = null;
    private RadioButton radioButtonColorPicker3 = null;
    private RadioButton radioButtonColorPicker4 = null;
    private NumberPicker numberPickerMax = null;
    private TextView txtViewMinTime = null;
    private NumberPicker numberPickerMin = null;
    private TextView txtViewMaxTime = null;
    private Switch switchRandomTime = null;
    private EditText edtTextSensor = null;
    BluetoothCommunication bluetoothCommunication = null;

    private SingletonClass singletonClass = SingletonClass.getInstance();

    /**
     * Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_settings);
        setUpViewItems();
        int returnInt = -1;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            returnInt = extras.getInt("DEVICE_ID");
            if (returnInt != -1) {


                bluetoothCommunication = BluetoothCommunication.getInstance();

                ArrayList<DeviceObjectClass> deviceObjectClassArrayList = new ArrayList<>();
                deviceObjectClassArrayList = singletonClass.getDeviceObjectClassArrayList();
                txtTitle.setText(deviceObjectClassArrayList.get(returnInt).getDeviceName());
                /*if(bluetoothCommunication.connectToDevice(deviceObjectClassArrayList.get(returnInt).getMAC())){
                    bluetoothCommunication.sendData("LED:1");

                }else{

                    finish();
                }*/
            }
        }

    }

    @Override
    protected void onStop() {
        bluetoothCommunication.disconnectToDevice();
        super.onStop();
    }

    /**
     * Set Up View Objects
     */
    private void setUpViewItems() {
        //TODO: get device settings

        txtTitle = (TextView) findViewById(R.id.txtViewTitleForActivityDeviceSettingsDeviceTitle);
        checkOnOff = (CheckBox) findViewById(R.id.checkBox);
        checkOnOff.setOnCheckedChangeListener(DeviceSettingsActivity.this);

        radioButtonColorPicker1 = (RadioButton) findViewById(R.id.radioBtnColorPicker1);
        radioButtonColorPicker1.setOnCheckedChangeListener(DeviceSettingsActivity.this);

        radioButtonColorPicker2 = (RadioButton) findViewById(R.id.radioBtnColorPicker2);
        radioButtonColorPicker2.setOnCheckedChangeListener(DeviceSettingsActivity.this);

        radioButtonColorPicker3 = (RadioButton) findViewById(R.id.radioBtnColorPicker3);
        radioButtonColorPicker3.setOnCheckedChangeListener(DeviceSettingsActivity.this);

        radioButtonColorPicker4 = (RadioButton) findViewById(R.id.radioBtnColorPicker4);
        radioButtonColorPicker4.setOnCheckedChangeListener(DeviceSettingsActivity.this);

        txtViewMinTime = (TextView) findViewById(R.id.txtViewMinTime);

        edtTextSensor = (EditText) findViewById(R.id.edtextSensor);

        numberPickerMin = (NumberPicker) findViewById(R.id.numberPicker2);
        numberPickerMin.setOnValueChangedListener(DeviceSettingsActivity.this);
        numberPickerMin.setMinValue(1);
        numberPickerMin.setMaxValue(99);
        numberPickerMin.setWrapSelectorWheel(true);

        txtViewMaxTime = (TextView) findViewById(R.id.txtViewMaxTime);

        numberPickerMax = (NumberPicker) findViewById(R.id.numberPicker3);
        numberPickerMax.setOnValueChangedListener(DeviceSettingsActivity.this);
        numberPickerMax.setMinValue(1);
        numberPickerMax.setMaxValue(99);
        numberPickerMax.setWrapSelectorWheel(true);

        switchRandomTime = (Switch) findViewById(R.id.switch1);
        switchRandomTime.setOnCheckedChangeListener(DeviceSettingsActivity.this);

    }


    /**
     * Switch
     * CheckBox
     *
     * @param buttonView
     * @param isChecked
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {
            case R.id.radioBtnColorPicker1:
                buttonView.setChecked(isChecked);
                break;

            case R.id.radioBtnColorPicker2:
                buttonView.setChecked(isChecked);
                break;

            case R.id.radioBtnColorPicker3:
                buttonView.setChecked(isChecked);
                break;

            case R.id.radioBtnColorPicker4:
                buttonView.setChecked(isChecked);
                break;

            case R.id.checkBox:
                radioButtonColorPicker4.setChecked(isChecked);
                buttonView.setChecked(isChecked);

                if (isChecked) {
                    switchRandomTime.setChecked(!isChecked);
                    numberPickerMin.setVisibility(View.VISIBLE);
                    txtViewMaxTime.setVisibility(View.GONE);
                    numberPickerMax.setVisibility(View.GONE);
                } else {
                    switchRandomTime.setChecked(!isChecked);
                    txtViewMinTime.setText(R.string.number_picker_text_min);
                    numberPickerMin.setVisibility(View.VISIBLE);
                    txtViewMaxTime.setVisibility(View.VISIBLE);
                    txtViewMaxTime.setText(R.string.number_picker_text_max);
                    numberPickerMax.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.switch1:
                switchRandomTime.setChecked(isChecked);
                if (!isChecked) {
                    txtViewMinTime.setText(R.string.number_picker_text_set_time);
                    numberPickerMin.setVisibility(View.VISIBLE);
                    txtViewMaxTime.setVisibility(View.GONE);
                    numberPickerMax.setVisibility(View.GONE);
                } else {
                    txtViewMinTime.setText(R.string.number_picker_text_min);
                    numberPickerMin.setVisibility(View.VISIBLE);
                    txtViewMaxTime.setVisibility(View.VISIBLE);
                    txtViewMaxTime.setText(R.string.number_picker_text_max);
                    numberPickerMax.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    /**
     * NumberPicker
     *
     * @param picker
     * @param oldVal
     * @param newVal
     */
    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

        picker.setValue(newVal);
        if (picker.getId() == R.id.numberPicker2) {
            numberPickerMax.setMinValue(newVal);
        }
    }

}
