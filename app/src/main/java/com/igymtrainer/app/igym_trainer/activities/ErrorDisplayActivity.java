package com.igymtrainer.app.igym_trainer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.igymtrainer.app.igym_trainer.R;

public class ErrorDisplayActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtViewForErrorText = null;
    private Button btnDone = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_display);
        setUpViewItems();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String returnString = extras.getString("ERROR_CODE");
            if (returnString != null) {
                if (returnString.equals("No_Bluetooth")) {
                    txtViewForErrorText.setText(getString(R.string.string_error_have_device_paired));
                } else if (returnString.equals("Bluetooth_close")) {
                    txtViewForErrorText.setText(getString(R.string.string_error_bluetooth_is_ont_open));
                } else if (returnString.equals("Bluetooth_No_Pair")) {
                    txtViewForErrorText.setText(getString(R.string.string_error_have_device_paired));
                }
            } else {
                goBack();
            }
        }
    }

    /**
     * Called when the activity is started and initialise view objects.
     */
    private void setUpViewItems() {
        txtViewForErrorText = (TextView) findViewById(R.id.txtViewForErrors);
        btnDone = (Button) findViewById(R.id.btn_go_back_to_main_activity_from_error_screen);
        btnDone.setOnClickListener(this);
    }

    /**
     * Called when the you wont close that activity.
     */
    private void goBack() {
        startActivity(new Intent(ErrorDisplayActivity.this, MainActivity.class));
        finish();
    }

    /**
     * Called when the button click and go Back.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == btnDone.getId()) {
            goBack();
        }
    }


}
