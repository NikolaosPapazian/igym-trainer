package com.igymtrainer.app.igym_trainer.device;

/**
 * Created by Papazian on 8/10/2017.
 * <p>
 * DeviceObjectClass
 * mast set Device Name and Device MAC
 */
public class DeviceObjectClass {

    private String deviceName = "";
    private int deviceIndex = 1;
    private String MAC = null;
    private boolean isCorrectDevice = false;
    private boolean isConnected = false;

    private int power = 40; // Battery of device
    private int powerPhoto = 3; // Battery of device
    private int color = 4; // color of device
    private int time = 1;
    private int minTime = 1; // minimum time for random
    private int maxTime = 2; // maximum time for random
    private boolean randomTime = false; // is random time on
    private boolean autoOnOff = false; // is auto on off on

    /**
     * DeviceObjectClass Construction
     * Set values to deviceMac, deviceName, deviceIndex.
     * if same device hasn't that values is not working
     *
     * @param MAC        string if not set it MAC=null
     * @param deviceName string if not set it deviceName=null
     */
    public DeviceObjectClass(String MAC, String deviceName) {

        if (MAC.length() == 17) {
            this.MAC = MAC;
            int index = deviceName.indexOf("r-");
            if (index != -1) {
                this.deviceName = deviceName.substring(0, index + 2);
                this.deviceIndex = Integer.parseInt(deviceName.substring(index + 1, deviceName.length()));
                if (this.deviceName.equals("IGym-Trainer-")) {
                    isCorrectDevice = true;
                }
            }
        } else {
            this.MAC = null;
            this.deviceName = null;
            this.deviceIndex = -1;
        }
    }

    /**
     * Getter for get all of Device Name with device index
     *
     * @return deviceName string
     */
    public String getDeviceName() {
        return deviceName + deviceIndex;
    }

    /**
     * getter for get Device Mac
     *
     * @return MAC string
     */
    public String getMAC() {
        return MAC;
    }

    public boolean isCorrectDevice() {
        return isCorrectDevice;
    }

    public void setCorrectDevice(boolean correctDevice) {
        isCorrectDevice = correctDevice;
    }

    /**
     * getter for get the status of connection for that device
     *
     * @return true if device is paired
     */
    public boolean isConnected() {
        return isConnected;
    }

    /**
     * get the power of this device
     *
     * @return power% int
     */
    public int getPower() {
        return power;
    }

    /**
     * get the correct foto index for showing to UI
     *
     * @return powerPhoto
     */
    public int getPowerPhoto() {
        return powerPhoto;
    }

    /**
     * get the color of that device
     *
     * @return color int
     */
    public int getColor() {
        return color;
    }

    /**
     * get the time for delay of device
     *
     * @return time int
     */
    public int getTime() {
        return time;
    }

    /**
     * get the min time for custom random
     *
     * @return minTime int
     */
    public int getMinTime() {
        return minTime;
    }

    /**
     * get the max time for custom random value
     *
     * @return maxTime int
     */
    public int getMaxTime() {
        return maxTime;
    }

    /**
     * get the status of custom random value
     *
     * @return true if random time is open
     */
    public boolean isRandomTime() {
        return randomTime;
    }

    /**
     * get the status of custom AutoOnOff mode
     *
     * @return true if AutoOnOff in on
     */
    public boolean isAutoOnOff() {
        return autoOnOff;
    }

    /**
     * set value to status of device connection
     *
     * @param connected boolean
     */
    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    /**
     * set value to power% of device and powerPhoto
     *
     * @param power int
     */
    public void setPower(int power) {
        this.power = power;
        setPowerPhoto(power);
    }

    /**
     * that set powerPhoto analog of power%
     *
     * @param power int
     */
    private void setPowerPhoto(int power) {
        if (power > 80) {
            this.powerPhoto = 5;
        } else if (power > 60) {
            this.powerPhoto = 4;
        } else if (power > 40) {
            this.powerPhoto = 3;
        } else if (power > 20) {
            this.powerPhoto = 2;
        } else if (power > 0) {
            this.powerPhoto = 1;
        }
    }

    /**
     * set color
     * if color is not in bounts of(1) to (4) set it (1)
     * color = 1 = Red
     * color = 2 = Green
     * color = 3 = blue
     * color = 4 = Off
     *
     * @param color int
     */
    public void setColor(int color) {
        if (color > 0 && color < 5) {
            this.color = color;
        } else {
            this.color = 1;
        }
    }

    /**
     * set time
     *
     * @param time int
     */
    public void setTime(int time) {
        this.time = time;
    }

    /**
     * set minTime to custom random function
     *
     * @param minTime int
     */
    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    /**
     * set maxTime to custom Random function
     *
     * @param maxTime int
     */
    public void setMaxTime(int maxTime) {
        if (this.minTime > maxTime) {
            this.maxTime = maxTime;
        }
    }

    /**
     * set random is true or false
     *
     * @param randomTime boolean
     */
    public void setRandomTime(boolean randomTime) {
        this.randomTime = randomTime;
    }

    /**
     * set AutoOnOff value
     *
     * @param autoOnOff boolean
     */
    public void setAutoOnOff(boolean autoOnOff) {
        this.autoOnOff = autoOnOff;
    }
}
